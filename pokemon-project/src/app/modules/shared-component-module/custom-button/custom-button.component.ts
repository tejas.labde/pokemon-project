import { FormGroup } from '@angular/forms';
import { Component, HostBinding, Input, OnInit } from '@angular/core';


@Component({
  selector: 'app-custom-button',
  templateUrl: './custom-button.component.html',
  styleUrls: ['./custom-button.component.scss']
})
export class CustomButtonComponent implements OnInit {

  @Input() form!:FormGroup;
  @Input() buttonText:string='';
  @HostBinding('form.valid') get valid(){
    return this.form.valid;
  }
  
  constructor() { }

  ngOnInit(): void {
  }

}
