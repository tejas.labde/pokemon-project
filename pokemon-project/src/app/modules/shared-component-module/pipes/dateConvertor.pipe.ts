import {Pipe,PipeTransform} from '@angular/core';

@Pipe({
    name:'dateConvertor'
})

export class DateConvertor implements PipeTransform{
    transform(value: any, ...args: any[]) {
        let date=new Date(value);
        return date.toLocaleTimeString();
        
    }
}