import { Component, forwardRef, Input, OnInit } from '@angular/core';
import { ControlValueAccessor,NG_VALUE_ACCESSOR} from '@angular/forms';

@Component({
  selector: 'app-custom-input',
  templateUrl: './custom-input.component.html',
  styleUrls: ['./custom-input.component.scss'],
  providers: [
    {
      provide:NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CustomInputComponent),
      multi:true
    }
  ]
})

export class CustomInputComponent implements OnInit, ControlValueAccessor {

  @Input() placeholder: string = ''
  @Input() public type:string='';
  @Input()  public labelFor:string='';
  value:string = '';

  onChange!:(value:string)=>void;
  onTouched!:()=>void;

  constructor() { }

  writeValue(obj: any): void {
    this.value=obj;
  }
  registerOnChange(fn: any): void {
    this.onChange=fn;
  }
  registerOnTouched(fn: any): void {
    this.onTouched=fn;
  }

  ngOnInit(): void {
  }


}
