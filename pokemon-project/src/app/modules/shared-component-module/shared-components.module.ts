import { DateConvertor } from './pipes/dateConvertor.pipe';
import { MatButtonModule } from '@angular/material/button';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CustomButtonComponent } from './custom-button/custom-button.component';
import { CustomInputComponent } from "./custom-input/custom-input.component";
import { NgModule } from "@angular/core";


@NgModule({
    declarations:[
        CustomButtonComponent,
        CustomInputComponent,
        DateConvertor
    ],
    imports:[
        FormsModule,
       
        ReactiveFormsModule,
        MatButtonModule
    ],
    exports:[
        CustomButtonComponent,
        CustomInputComponent,
        DateConvertor,
    ],
    providers:[]

})

export class SharedComponentsModule{}