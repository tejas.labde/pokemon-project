import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthorisationModuleRoutingModule } from './authorisation-module-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AuthorisationModuleRoutingModule
  ]
})
export class AuthorisationModuleModule { }
