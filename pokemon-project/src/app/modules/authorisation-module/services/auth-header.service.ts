import { Observable } from 'rxjs';
import { AppHttpInterceptorService } from 'src/app/service/app-http-interceptor.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthHeaderService {

  constructor(private httpClient:HttpClient) { }

  loginUser(){
    return this.httpClient.post('http://localhost:3000/auth/login',{
      email:'professor.oak@pokedex.com',
      password:'pikapi'
    });
  }
}
