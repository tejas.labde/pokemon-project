import { AuthHeaderService } from '../../services/auth-header.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {

  token:string='';

  constructor(public fb: FormBuilder,public authService:AuthHeaderService,private router:Router) { }

  loginForm: FormGroup = this.fb.group({
    email:new FormControl('', [Validators.required, Validators.email]),
    password:new FormControl('',[Validators.required, Validators.minLength(4)])
  });
  formSubmitted:boolean=false;
  ngOnInit(): void {
  }

  onSubmit(loginForm:FormGroup){
    if(loginForm.valid)
    {
      alert('form is valid');
     this.authService.loginUser().subscribe(response=>{
      console.log(response);
      this.token=Object(response).data.token;
      localStorage.setItem('token',this.token);
      this.router.navigate(['home']);
     });
     
  }
    else this.formSubmitted=false;
  }
}

