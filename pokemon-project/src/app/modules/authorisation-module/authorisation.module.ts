import { SharedComponentsModule } from './../shared-component-module/shared-components.module';
import { LoginPageComponent } from './pages/login-page/login-page.component';

import { MatButtonModule } from '@angular/material/button';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthorisationModuleRoutingModule } from './authorisation-module/authorisation-module-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from "@angular/core";

@NgModule({
    declarations:[
        LoginPageComponent
    ],
    imports:[
        FormsModule,
        ReactiveFormsModule,
        SharedComponentsModule,
        MatButtonModule,
        AuthorisationModuleRoutingModule,
        HttpClientModule
    ],
    providers:[],
    bootstrap:[LoginPageComponent]

})

export class AuthorisationModule{}
