

export interface IpokemonType{
    name:string,
    type:number,
    level:number,
    dateCreated:Date,
    dateUpdated:Date ,
    id:number,
    isSelected:false
}
