import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PokemonModuleRoutingModule } from './pokemon-module-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    PokemonModuleRoutingModule
  ]
})
export class PokemonModuleModule { }
