import { MatFormFieldModule } from '@angular/material/form-field';
import { DateConvertor } from './../shared-component-module/pipes/dateConvertor.pipe';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HomePageComponent } from '../pokemon-module/components/home-page/home-page.component';
import { NgModule } from "@angular/core";
import {MatTableModule} from '@angular/material/table';
import { PokemonModuleRoutingModule } from './pokemon-module/pokemon-module-routing.module';
import { SharedComponentsModule } from '../shared-component-module/shared-components.module';
import { PokemonDetailsComponent } from './pokemon-details/pokemon-details.component';
import {MatDialogModule} from '@angular/material/dialog';

@NgModule({
    declarations:[
        HomePageComponent,
        PokemonDetailsComponent
    ],
    imports:[
        MatTableModule,
        MatDialogModule,
        MatFormFieldModule,
        
        SharedComponentsModule,
        FormsModule,
        ReactiveFormsModule,
        PokemonModuleRoutingModule
    ],
    providers:[],
    bootstrap:[HomePageComponent]
})

export class PokemonModule{};