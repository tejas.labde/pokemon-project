import { IpokemonType } from './../pokemonList.types';
import { PokemonService } from './../services/pokemon.service';
import { FormBuilder,FormGroup,FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pokemon-details',
  templateUrl: './pokemon-details.component.html',
  styleUrls: ['./pokemon-details.component.scss']
})
export class PokemonDetailsComponent implements OnInit {

  constructor(private fb:FormBuilder,private pokeService:PokemonService) { }



  

  pokeUpdate: FormGroup = this.fb.group({
    name:new FormControl(''),
    type:new FormControl(0),
    level:new FormControl(0),
    isSelected:true
  });

  data!:IpokemonType;

  ngOnInit(): void {}

  onUpdate(pokeUpdate:FormGroup){
    alert('pokemon update recorded');
    // this.data={...Object(pokeUpdate)};
    console.log('sending this data',Object(pokeUpdate).value);
    this.pokeService.addPokemon(pokeUpdate.value).subscribe(response=>{
      console.log('response on update',response);
    })
  }

  

}
