import { IpokemonType } from './../pokemonList.types';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PokemonService {

  constructor(private httpClient:HttpClient) { }

  getPokemon(){
    return this.httpClient.get('http://localhost:3000/pokemon');
  }

  addPokemon(updatedDetails:any){
    console.log(updatedDetails);
    updatedDetails=JSON.stringify(updatedDetails);
    return this.httpClient.post('http://localhost:3000/pokemon',updatedDetails)
  }
}
