import { PokemonDetailsComponent } from './../../pokemon-details/pokemon-details.component';
import { Component, OnInit } from '@angular/core';
import { PokemonService } from '../../services/pokemon.service';
import { IpokemonType } from '../../pokemonList.types';
import { MatDialog } from '@angular/material/dialog';


@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {

  //add interface according to the data which is being sent from the backend
  pokeData:IpokemonType[]=[];

  columnsToDisplay:string[]=['name','level','type','dateCreated','dateUpdated','editBtnColumn','deleteBtnColumn']
  
  constructor(private pokeService:PokemonService,public dialog:MatDialog) { }

  ngOnInit(): void {
    //getPokemonData function will be called as soon as the component is initialised.
    this.getPokemonData();
  }

  openDialog(){
    let dialogRef=this.dialog.open(PokemonDetailsComponent);
    dialogRef.afterClosed().subscribe(resp=>{
      console.log(resp);
    })
  }

  getPokemonData(){
    this.pokeService.getPokemon().subscribe(response=>{
      //console logging the response we get from the backend containing the details of the pokemons.
      console.log('pokemon data',response);
      this.pokeData=Object(response).data;
      console.log('pokedata',this.pokeData);
    })
  }


}
