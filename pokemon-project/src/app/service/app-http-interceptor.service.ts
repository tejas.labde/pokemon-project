import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AppHttpInterceptorService implements HttpInterceptor{

  token:string=localStorage.getItem('token') || '';
  constructor() { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    console.log(req);
    //check if incoming request has headers and incase it doesn't then set the value of the headers using .set method 
    if(!req.headers.has('ContentType')){
      req=req.clone({headers:req.headers.set('Content-Type','application/json')});
    }

    if(!req.headers.has('Authorization')){
      req=req.clone({headers:req.headers.set('Authorization', this.token)})
    }
    return next.handle(req);
  }
}
