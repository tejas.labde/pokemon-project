import { AuthorisationModule } from './modules/authorisation-module/authorisation.module';
import { PokemonModule } from './modules/pokemon-module/pokemon.module';
import { SharedComponentsModule } from './modules/shared-component-module/shared-components.module';
import { AppHttpInterceptorService } from './service/app-http-interceptor.service';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule,FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {MatButtonModule} from '@angular/material/button';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ColorAlternatorDirective } from './directives/color-alternator.directive';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
@NgModule({
  declarations: [
   
    AppComponent,
    ColorAlternatorDirective,
  ],
  imports: [
    BrowserModule,
    SharedComponentsModule,
    PokemonModule,
    AuthorisationModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    AppRoutingModule,
    MatButtonModule,
    BrowserAnimationsModule
  ],
  providers: [
    {
      provide:HTTP_INTERCEPTORS,
      useClass:AppHttpInterceptorService,
      multi:true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
