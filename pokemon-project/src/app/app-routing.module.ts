
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [{
  path:'',
    loadChildren:()=>
    import('./modules/authorisation-module/authorisation.module')
    .then(m=>m.AuthorisationModule)
},
{
  path:'home',
  loadChildren:()=>
  import('./modules/pokemon-module/pokemon.module')
  .then(m=>m.PokemonModule)
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
