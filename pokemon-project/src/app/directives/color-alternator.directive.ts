import { Directive ,ElementRef, HostListener} from '@angular/core';

@Directive({
  selector: '[appColorAlternator]'
})
export class ColorAlternatorDirective {

  constructor(public el:ElementRef) { }
  @HostListener('mouseover') onMouseHover(){
    this.el.nativeElement.style.backgroundColor='rgb(63, 81, 181)';
    this.el.nativeElement.style.color='white'
  }
  @HostListener('mouseout') onMouseHoverAway(){
    this.el.nativeElement.style.backgroundColor='white';
    this.el.nativeElement.style.color='rgb(63, 81, 181)'
  }
}
